variable "cname" {}
variable "global_name_map" { type = map}
variable "lb_name" {}
variable "ssl_certificate_arn" {}
variable "tags" {}
variable "target_group_prefix" {}
variable "type" {
  type = string
  default = "forward"
  }
variable "vpc_id" {}
variable "deregistration_delay" { default = 300 }


