data "aws_lb" "lb" {
  name = var.lb_name
}
resource "aws_alb_listener" "alb_listener" {
    for_each          = var.global_name_map
    load_balancer_arn = data.aws_lb.lb.arn
    port              = each.value["balancer_port"]
    protocol          = each.value["source_protocol"]
    ssl_policy        = each.value["source_protocol"] != "HTTPS" ? null : "ELBSecurityPolicy-2016-08"
    certificate_arn   = each.value["source_protocol"] != "HTTPS" ? null : var.ssl_certificate_arn
    default_action {
      type = "redirect" 
      redirect {
          port = each.value["destination_port"]
          protocol = each.value["destination_protocol"]
          status_code = "HTTP_301"
      }
    }
}
